package postgres

import (
	"context"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/go/sqlutils/sqltesting"
	"gitlab.com/witchbrew/september/go/ask-service/grpc"
	"gitlab.com/witchbrew/september/go/ask-service/repository/postgres"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"gitlab.com/witchbrew/september/go/askgrpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestGRPC_OpenEndedCreateGetDelete(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewQuestionRepository(db)
	s := grpc.NewTestServer(t, service.NewQuestionService(idconv.ConverterUint64, r))
	defer s.Stop()
	c := grpc.NewTestClient(t)
	w, err := c.Create(context.Background(), &askgrpc.CreateQuestion{
		Text: "What's up?",
	})
	require.Nil(t, err)
	require.NotEqual(t, "", w.ID)
	defer func() {
		_, err := c.Delete(context.Background(), &askgrpc.AskIDWrapper{ID: w.ID})
		require.Nil(t, err)
		q, err := c.Get(context.Background(), &askgrpc.AskIDWrapper{ID: w.ID})
		require.NotNil(t, err)
		require.Nil(t, q)
		st, ok := status.FromError(err)
		require.True(t, ok)
		require.Equal(t, codes.NotFound, st.Code())
	}()
	q, err := c.Get(context.Background(), &askgrpc.AskIDWrapper{ID: w.ID})
	require.Nil(t, err)
	require.Equal(t, w.ID, q.ID)
	require.Equal(t, "What's up?", q.Text)
}

func TestGRPC_CloseEndedCreateGetDelete(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewQuestionRepository(db)
	s := grpc.NewTestServer(t, service.NewQuestionService(idconv.ConverterUint64, r))
	defer s.Stop()
	c := grpc.NewTestClient(t)
	w, err := c.Create(context.Background(), &askgrpc.CreateQuestion{
		Text:    "How are you?",
		Options: []string{"good", "so-so", "bad"},
	})
	require.Nil(t, err)
	require.NotEqual(t, "", w.ID)
	defer func() {
		_, err := c.Delete(context.Background(), &askgrpc.AskIDWrapper{ID: w.ID})
		require.Nil(t, err)
		q, err := c.Get(context.Background(), &askgrpc.AskIDWrapper{ID: w.ID})
		require.NotNil(t, err)
		require.Nil(t, q)
		st, ok := status.FromError(err)
		require.True(t, ok)
		require.Equal(t, codes.NotFound, st.Code())
	}()
	q, err := c.Get(context.Background(), &askgrpc.AskIDWrapper{ID: w.ID})
	require.Nil(t, err)
	require.Equal(t, w.ID, q.ID)
	require.Equal(t, "How are you?", q.Text)
	require.Equal(t, []string{"good", "so-so", "bad"}, q.Options)
}
