package postgres

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/go/sqlutils/sqltesting"
	"gitlab.com/witchbrew/september/go/ask-service/model/question"
	"gitlab.com/witchbrew/september/go/ask-service/repository/postgres"
	"os"
	"testing"
)

func newDB(t *testing.T) *sql.DB {
	user := os.Getenv("TEST_PG_USERNAME")
	password := os.Getenv("TEST_PG_PASSWORD")
	host := os.Getenv("TEST_PG_HOST")
	database := os.Getenv("TEST_PG_DATABASE")
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		password,
		host,
		database,
	)
	db, err := sql.Open("postgres", connStr)
	require.Nil(t, err)
	return db

}

func TestOpenEndedNonExistentID(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewQuestionRepository(db)
	nonExistentID := id.New()
	nonExistentID.Value = 100500
	nonExistentQuestion, err := r.Get(nonExistentID)
	require.Nil(t, err)
	require.Nil(t, nonExistentQuestion)
}

func TestOpenEndedCreateGetDelete(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewQuestionRepository(db)
	q := question.New("What's up?")
	err := r.Create(q)
	require.Nil(t, err)
	require.True(t, q.ID().IsInitialized())
	defer func() {
		err := r.Delete(q.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(q.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	q2, err := r.Get(q.ID())
	require.Nil(t, err)
	require.NotNil(t, q2)
	require.Equal(t, q, q2)
	qm, err := r.GetMeta(q.ID())
	require.Nil(t, err)
	require.Equal(t, qm.ID(), q.ID())
	require.Equal(t, 0, qm.Options())
}

func TestCloseEndedCreateGetDelete(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewQuestionRepository(db)
	q := question.New("How are you?", "good", "so-so", "bad")
	err := r.Create(q)
	require.Nil(t, err)
	require.True(t, q.ID().IsInitialized())
	defer func() {
		err := r.Delete(q.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(q.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	q2, err := r.Get(q.ID())
	require.Nil(t, err)
	require.NotNil(t, q2)
	require.Equal(t, q, q2)
	qm, err := r.GetMeta(q.ID())
	require.Nil(t, err)
	require.Equal(t, qm.ID(), q.ID())
	require.Equal(t, 3, qm.Options())
}
