package grpc

import (
	"context"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/errorhandling"
	"gitlab.com/witchbrew/go/grpcutils/grpcerrors"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/september/go/ask-service/repository/inmemory"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"gitlab.com/witchbrew/september/go/askgrpc"
	"testing"
)

func TestCreateGetDelete_OpenEnded(t *testing.T) {
	r := inmemory.NewQuestionRepository()
	gRPCServer := NewTestServer(t, service.NewQuestionService(idconv.ConverterUint64, r))
	defer gRPCServer.Stop()
	gRPCClient := NewTestClient(t)
	w, err := gRPCClient.Create(context.Background(), &askgrpc.CreateQuestion{
		Text: "What's up?",
	})
	require.Nil(t, err)
	require.Equal(t, "1", w.ID)
	defer func() {
		_, err := gRPCClient.Delete(context.Background(), &askgrpc.AskIDWrapper{ID: "1"})
		require.Nil(t, err)
		q, err := gRPCClient.Get(context.Background(), &askgrpc.AskIDWrapper{ID: "1"})
		require.NotNil(t, err)
		require.Nil(t, q)
		hErr := grpcerrors.ToHandledError(err)
		require.Equal(t, errorhandling.NotFound, hErr.Type)
		require.Contains(t, hErr.Message, `question with ID="1" does not exist`)
	}()
	q, err := gRPCClient.Get(context.Background(), &askgrpc.AskIDWrapper{ID: "1"})
	require.Nil(t, err)
	require.Equal(t, "1", q.ID)
	require.Equal(t, "What's up?", q.Text)
}

func TestCreateGetDelete_CloseEnded(t *testing.T) {
	r := inmemory.NewQuestionRepository()
	gRPCServer := NewTestServer(t, service.NewQuestionService(idconv.ConverterUint64, r))
	defer gRPCServer.Stop()
	gRPCClient := NewTestClient(t)
	w, err := gRPCClient.Create(context.Background(), &askgrpc.CreateQuestion{
		Text:    "How are you?",
		Options: []string{"good", "so-so", "bad"},
	})
	require.Nil(t, err)
	require.Equal(t, "1", w.ID)
	defer func() {
		_, err := gRPCClient.Delete(context.Background(), &askgrpc.AskIDWrapper{ID: "1"})
		require.Nil(t, err)
		q, err := gRPCClient.Get(context.Background(), &askgrpc.AskIDWrapper{ID: "1"})
		require.NotNil(t, err)
		require.Nil(t, q)
		hErr := grpcerrors.ToHandledError(err)
		require.Equal(t, errorhandling.NotFound, hErr.Type)
		require.Contains(t, hErr.Message, `question with ID="1" does not exist`)
	}()
	q, err := gRPCClient.Get(context.Background(), &askgrpc.AskIDWrapper{ID: "1"})
	require.Nil(t, err)
	require.Equal(t, "1", q.ID)
	require.Equal(t, "How are you?", q.Text)
	require.Equal(t, []string{"good", "so-so", "bad"}, q.Options)
}
