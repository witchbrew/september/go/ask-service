package grpc

import (
	"context"
	"gitlab.com/witchbrew/go/grpcutils/grpcerrors"
	"gitlab.com/witchbrew/go/grpcutils/server_interceptors"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"gitlab.com/witchbrew/september/go/askgrpc"
	"google.golang.org/grpc"
)

type askServer struct {
	askgrpc.UnimplementedAskServer
	questionService *service.Question
}

func (s *askServer) Create(ctx context.Context, q *askgrpc.CreateQuestion) (*askgrpc.AskIDWrapper, error) {
	options := make([]string, 0)
	if len(q.Options) > 0 {
		options = q.Options
	}
	qDTO, err := s.questionService.Create(q.Text, options...)
	if err != nil {
		return nil, grpcerrors.Process(err)
	}
	return &askgrpc.AskIDWrapper{ID: qDTO.ID}, nil
}

func (s *askServer) Get(ctx context.Context, wrapper *askgrpc.AskIDWrapper) (*askgrpc.GetQuestion, error) {
	qDTO, err := s.questionService.Get(wrapper.ID)
	if err != nil {
		return nil, grpcerrors.Process(err)
	}
	resp := &askgrpc.GetQuestion{
		ID:   qDTO.ID,
		Text: qDTO.Text,
	}
	if qDTO.HasOptions() {
		resp.Options = qDTO.Options
	}
	return resp, nil
}

func (s *askServer) GetMeta(ctx context.Context, wrapper *askgrpc.AskIDWrapper) (*askgrpc.QuestionMeta, error) {
	qm, err := s.questionService.GetMeta(wrapper.ID)
	if err != nil {
		return nil, grpcerrors.Process(err)
	}
	return &askgrpc.QuestionMeta{
		ID:      qm.ID,
		Options: int32(qm.Options),
	}, nil
}

func (s *askServer) Delete(ctx context.Context, wrapper *askgrpc.AskIDWrapper) (*askgrpc.AskEmpty, error) {
	err := s.questionService.Delete(wrapper.ID)
	if err != nil {
		return nil, grpcerrors.Process(err)
	}
	return &askgrpc.AskEmpty{}, nil
}

func New(questionService *service.Question) (*grpc.Server, error) {
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		server_interceptors.RequestID,
		server_interceptors.Zerolog,
		server_interceptors.ZerologRequestID,
		server_interceptors.ZerologLogServerInfo,
	))
	askgrpc.RegisterAskServer(grpcServer, &askServer{questionService: questionService})
	return grpcServer, nil
}
