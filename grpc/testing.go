package grpc

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"gitlab.com/witchbrew/september/go/askgrpc"
	"google.golang.org/grpc"
	"net"
	"testing"
)

const port = ":50051"

func NewTestServer(t *testing.T, serv *service.Question) *grpc.Server {
	listener, err := net.Listen("tcp", port)
	require.Nil(t, err)
	gRPCServer, err := New(serv)
	require.Nil(t, err)
	go func() {
		err := gRPCServer.Serve(listener)
		require.Nil(t, err)
	}()
	return gRPCServer
}

func NewTestClient(t *testing.T) askgrpc.AskClient {
	clientConn, err := grpc.Dial(fmt.Sprintf("localhost%s", port), grpc.WithInsecure(), grpc.WithBlock())
	require.Nil(t, err)
	return askgrpc.NewAskClient(clientConn)
}
