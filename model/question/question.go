package question

import (
	"gitlab.com/witchbrew/go/idutils/id"
)

type Question struct {
	id      *id.ID
	text    string
	options []string
}

func (q *Question) ID() *id.ID {
	return q.id
}

func (q *Question) Text() string {
	return q.text
}

func (q *Question) HasOptions() bool {
	return len(q.options) != 0
}

func (q *Question) Options() []string {
	return q.options
}

func New(text string, options ...string) *Question {
	q := &Question{
		id:      id.New(),
		text:    text,
		options: nil,
	}
	if len(options) == 1 {
		panic("total number of options must be 2 or more")
	}
	if options == nil {
		options = make([]string, 0)
	}
	q.options = options
	return q
}
