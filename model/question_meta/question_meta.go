package question_meta

import (
	"gitlab.com/witchbrew/go/idutils/id"
)

type QuestionMeta struct {
	id      *id.ID
	options int
}

func (q *QuestionMeta) ID() *id.ID {
	return q.id
}

func (q *QuestionMeta) Options() int {
	return q.options
}

func New(options int) *QuestionMeta {
	if options < 0 {
		panic("number of options can't be negative")
	}
	if options == 1 {
		panic("number of options must be 2 or more")
	}
	return &QuestionMeta{
		id:      id.New(),
		options: options,
	}
}
