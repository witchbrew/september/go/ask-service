package service

import (
	"gitlab.com/witchbrew/go/errorhandling"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/september/go/ask-service/model/question"
	"gitlab.com/witchbrew/september/go/ask-service/repository"
)

type QuestionDTO struct {
	ID      string
	Text    string
	Options []string
}

func (q *QuestionDTO) HasOptions() bool {
	return q.Options != nil
}

type Question struct {
	idConverter idconv.Converter
	repository  repository.Question
}

func (s *Question) Create(text string, options ...string) (*QuestionDTO, error) {
	if len(options) == 1 {
		return nil, errorhandling.BadInputError("at least 2 options must be provided")
	}
	q := question.New(text, options...)
	err := s.repository.Create(q)
	if err != nil {
		return nil, err
	}
	dto := &QuestionDTO{
		ID:      s.idConverter.ToString(q.ID()),
		Text:    text,
		Options: nil,
	}
	if q.HasOptions() {
		dto.Options = q.Options()
	}
	return dto, nil
}

func (s *Question) Get(id string) (*QuestionDTO, error) {
	i, err := s.idConverter.FromString(id)
	if err != nil {
		return nil, errorhandling.BadInputError(err.Error())
	}
	q, err := s.repository.Get(i)
	if err != nil {
		return nil, err
	}
	if q == nil {
		return nil, errorhandling.NotFoundErrorf(`question with ID="%s" does not exist`, id)
	}
	dto := &QuestionDTO{
		ID:      id,
		Text:    q.Text(),
		Options: nil,
	}
	if q.HasOptions() {
		dto.Options = q.Options()
	}
	return dto, nil
}

type QuestionMetaDTO struct {
	ID      string
	Options int
}

func (s *Question) GetMeta(id string) (*QuestionMetaDTO, error) {
	i, err := s.idConverter.FromString(id)
	if err != nil {
		return nil, errorhandling.BadInputError(err.Error())
	}
	qm, err := s.repository.GetMeta(i)
	if err != nil {
		return nil, err
	}
	if qm == nil {
		return nil, errorhandling.NotFoundErrorf(`question with ID="%s" does not exist`, id)
	}
	return &QuestionMetaDTO{
		ID:      id,
		Options: qm.Options(),
	}, nil
}

func (s *Question) Delete(id string) error {
	i, err := s.idConverter.FromString(id)
	if err != nil {
		return errorhandling.BadInputError(err.Error())
	}
	err = s.repository.Delete(i)
	if err != nil {
		return err
	}
	return nil
}

func NewQuestionService(idC idconv.Converter, repo repository.Question) *Question {
	return &Question{
		idConverter: idC,
		repository:  repo,
	}
}
