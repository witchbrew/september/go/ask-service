package sqlmetaimpl

import (
	coreSqlMeta "gitlab.com/witchbrew/go/sqlmeta"
	"gitlab.com/witchbrew/september/go/ask-service/sqlmeta"
)

var QuestionTable = &sqlmeta.QuestionTable{
	Table: coreSqlMeta.NewTable("questions"),
	Columns: &sqlmeta.QuestionColumns{
		ID:   "id",
		Text: "text",
	},
}

var OptionTable = &sqlmeta.OptionTable{
	Table: coreSqlMeta.NewTable("options"),
	Columns: &sqlmeta.OptionColumns{
		QuestionID: "question_id",
		Index:      "index",
		Text:       "text",
	},
}
