package sqlmeta

import "gitlab.com/witchbrew/go/sqlmeta"

type QuestionTable struct {
	*sqlmeta.Table
	Columns *QuestionColumns
}

type QuestionColumns struct {
	ID   string
	Text string
}

type OptionTable struct {
	*sqlmeta.Table
	Columns *OptionColumns
}

type OptionColumns struct {
	QuestionID string
	Index      string
	Text       string
}
