package cmd

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/witchbrew/go/configutils/db_type_config"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/september/go/ask-service/api"
	"gitlab.com/witchbrew/september/go/ask-service/config"
	"gitlab.com/witchbrew/september/go/ask-service/grpc"
	"gitlab.com/witchbrew/september/go/ask-service/repository"
	"gitlab.com/witchbrew/september/go/ask-service/repository/inmemory"
	"gitlab.com/witchbrew/september/go/ask-service/repository/postgres"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var appConfig *config.Config

func getConfig() (*config.Config, error) {
	if appConfig == nil {
		c, err := config.NewFromEnv()
		if err != nil {
			return nil, err
		}
		appConfig = c
	}
	return appConfig, nil
}

var rootCmd = &cobra.Command{
	Use: "ask-service",
	RunE: func(cmd *cobra.Command, args []string) error {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		conf, err := getConfig()
		if err != nil {
			return err
		}
		var idConverter idconv.Converter
		var questionRepository repository.Question
		switch conf.DBType {
		case db_type_config.Postgres:
			db, err := sql.Open("postgres", conf.Postgres.ConnectionString())
			if err != nil {
				return err
			}
			defer func() {
				err := db.Close()
				if err != nil {
					panic(err)
				}
			}()
			questionRepository = postgres.NewQuestionRepository(db)
			idConverter = idconv.ConverterUint64
		case db_type_config.InMemory:
			questionRepository = inmemory.NewQuestionRepository()
			idConverter = idconv.ConverterUint64
		default:
			return errors.Errorf(`cannot create question repository for DB type "%s"`, conf.DBType)
		}
		questionService := service.NewQuestionService(idConverter, questionRepository)
		httpAPIHandler := api.New(questionService)
		gRPCListener, err := net.Listen("tcp", conf.GRPCPort.ToLocalAddress())
		if err != nil {
			return err
		}
		gRPCServer, err := grpc.New(questionService)
		if err != nil {
			return err
		}
		go func() {
			err := http.ListenAndServe(conf.HTTPPort.ToLocalAddress(), httpAPIHandler)
			if err != nil {
				panic(err)
			}
		}()
		log.Logger.Info().Uint16("port", uint16(conf.HTTPPort)).Msg("serving HTTP")
		go func() {
			err := gRPCServer.Serve(gRPCListener)
			if err != nil {
				panic(err)
			}
		}()
		log.Logger.Info().Uint16("port", uint16(conf.GRPCPort)).Msg("serving gRPC")
		signalChannel := make(chan os.Signal)
		signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
		<-signalChannel
		return nil
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
