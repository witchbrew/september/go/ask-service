package api

import (
	"gitlab.com/witchbrew/go/http/httphandler"
	"gitlab.com/witchbrew/go/http/httprouter"
	"gitlab.com/witchbrew/go/jsonschema"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"net/http"
)

type (
	sch = jsonschema.Sch
	obj = jsonschema.Obj
	arr = jsonschema.Arr
)

var CreateQuestionSch = sch{
	"type":                 "object",
	"additionalProperties": false,
	"properties": obj{
		"text": obj{
			"type": "string",
		},
		"options": obj{
			"type": "array",
			"items": obj{
				"type": "string",
			},
			"minItems": 2,
		},
	},
	"required": arr{"text"},
}
var createQuestionSchema = CreateQuestionSch.MustCompile()

func convertValidationError(validationErr *jsonschema.ValidationError, message string) *httphandler.Error {
	values := httphandler.ErrorValues{}
	for _, pathErr := range validationErr.Errors {
		values[pathErr.StringPath(".")] = pathErr.Message
	}
	return &httphandler.Error{
		Type:    "validation",
		Message: message,
		Values:  values,
	}
}

func New(questionService *service.Question) http.Handler {
	r := httprouter.NewJSON()
	r.Post("/", func(context *httphandler.HandlerContext) {
		validationErr := createQuestionSchema.Validate(context.Request.Body())
		if validationErr != nil {
			httpErr := convertValidationError(validationErr, "Bad question object")
			context.Response.Error(http.StatusBadRequest, httpErr)
			return
		}
		questionDTO := context.Request.Body().(map[string]interface{})

		_, ok := questionDTO["options"]
		options := make([]string, 0)
		if ok {
			optionsDTO := questionDTO["options"].([]interface{})
			options = make([]string, len(optionsDTO))
			for index, option := range optionsDTO {
				options[index] = option.(string)
			}
		}
		q, err := questionService.Create(questionDTO["text"].(string), options...)
		if err != nil {
			context.Response.ProcessError(err)
			return
		}
		context.Response.Body(http.StatusCreated, obj{
			"ID": q.ID,
		})
	})
	r.Get("/{ID}", func(context *httphandler.HandlerContext) {
		q, err := questionService.Get(context.Request.URLParam("ID"))
		if err != nil {
			context.Response.ProcessError(err)
			return
		}
		body := obj{
			"ID":   q.ID,
			"text": q.Text,
		}
		if q.HasOptions() {
			bodyOptions := make(arr, len(q.Options))
			for index, option := range q.Options {
				bodyOptions[index] = option
			}
			body["options"] = bodyOptions
		}
		context.Response.Body(http.StatusOK, body)
	})
	r.Get("/{ID}/meta", func(context *httphandler.HandlerContext) {
		qm, err := questionService.GetMeta(context.Request.URLParam("ID"))
		if err != nil {
			context.Response.ProcessError(err)
			return
		}
		context.Response.Body(http.StatusOK, obj{
			"ID":      qm.ID,
			"options": qm.Options,
		})
	})
	r.Delete("/{ID}", func(context *httphandler.HandlerContext) {
		err := questionService.Delete(context.Request.URLParam("ID"))
		if err != nil {
			context.Response.ProcessError(err)
			return
		}
		context.Response.Empty(http.StatusNoContent)
	})
	return r
}
