package api

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/http/httptestclient"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/september/go/ask-service/repository/inmemory"
	"gitlab.com/witchbrew/september/go/ask-service/service"
	"net/http"
	"testing"
)

func TestCreateGetDelete_OpenEnded(t *testing.T) {
	s := service.NewQuestionService(idconv.ConverterUint64, inmemory.NewQuestionRepository())
	c := httptestclient.NewJSON(t, New(s))
	m := map[string]interface{}{
		"text": "What's up?",
	}
	resp := c.Post("/").Body(m).Send()
	require.Equal(t, map[string]interface{}{"ID": "1"}, resp.Body)
	require.Equal(t, http.StatusCreated, resp.StatusCode)
	require.Equal(t, "application/json", resp.Headers.Get("Content-Type"))
	defer func() {
		resp = c.Delete("/1").Send()
		require.Equal(t, http.StatusNoContent, resp.StatusCode)
		resp = c.Get("/1").Send()
		require.Equal(t, http.StatusNotFound, resp.StatusCode)
	}()
	resp = c.Get("/1").Send()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	m["ID"] = "1"
	require.Equal(t, m, resp.Body)
	resp = c.Get("/1/meta").Send()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	require.Equal(t, map[string]interface{}{
		"ID": "1",
		"options": 0.0,
	}, resp.Body)
}

func TestCreateGetDelete_CloseEnded(t *testing.T) {
	s := service.NewQuestionService(idconv.ConverterUint64, inmemory.NewQuestionRepository())
	c := httptestclient.NewJSON(t, New(s))
	m := map[string]interface{}{
		"text":    "How are you?",
		"options": []interface{}{"good", "so-so", "bad"},
	}
	resp := c.Post("/").Body(m).Send()
	require.Equal(t, map[string]interface{}{"ID": "1"}, resp.Body)
	require.Equal(t, http.StatusCreated, resp.StatusCode)
	require.Equal(t, "application/json", resp.Headers.Get("Content-Type"))
	require.Equal(t, map[string]interface{}{"ID": "1"}, resp.Body)
	defer func() {
		resp = c.Delete("/1").Send()
		require.Equal(t, http.StatusNoContent, resp.StatusCode)
		resp = c.Get("/1").Send()
		require.Equal(t, http.StatusNotFound, resp.StatusCode)
	}()
	resp = c.Get("/1").Send()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	m["ID"] = "1"
	require.Equal(t, m, resp.Body)
	resp = c.Get("/1/meta").Send()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	require.Equal(t, map[string]interface{}{
		"ID": "1",
		"options": 3.0,
	}, resp.Body)
}

func TestGet_BadID(t *testing.T) {
	s := service.NewQuestionService(idconv.ConverterUint64, inmemory.NewQuestionRepository())
	c := httptestclient.NewJSON(t, New(s))
	resp := c.Get("/bad-ID").Send()
	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
	require.Equal(t, map[string]interface{}{
		"type":    "informative",
		"message": `bad uint64 string: "bad-ID"`,
	}, resp.Body)
}

func TestCreate_SingleOption(t *testing.T) {
	s := service.NewQuestionService(idconv.ConverterUint64, inmemory.NewQuestionRepository())
	c := httptestclient.NewJSON(t, New(s))
	m := map[string]interface{}{
		"text":    "How are you?",
		"options": []interface{}{"good"},
	}
	resp := c.Post("/").Body(m).Send()
	require.Equal(t, map[string]interface{}{
		"message": "Bad question object",
		"type":    "validation",
		"values":  map[string]interface{}{"@root.options": "minimum 2 items allowed, but found 1 items"},
	}, resp.Body)
	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
	require.Equal(t, "application/json", resp.Headers.Get("Content-Type"))
}
