package inmemory

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/september/go/ask-service/model/question"
	"testing"
)

func TestOpenEndedCreateGetDelete(t *testing.T) {
	r := NewQuestionRepository()
	q := question.New("What's up?")
	err := r.Create(q)
	require.Nil(t, err)
	require.True(t, q.ID().IsInitialized())
	defer func() {
		err := r.Delete(q.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(q.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	q2, err := r.Get(q.ID())
	require.Nil(t, err)
	require.NotNil(t, q2)
	require.Equal(t, q, q2)
	qm, err := r.GetMeta(q.ID())
	require.Nil(t, err)
	require.Equal(t, qm.ID(), q.ID())
	require.Equal(t, 0, qm.Options())
}

func TestCloseEndedCreateGetDelete(t *testing.T) {
	r := NewQuestionRepository()
	q := question.New("How are you?", "good", "so-so", "bad")
	err := r.Create(q)
	require.Nil(t, err)
	require.True(t, q.ID().IsInitialized())
	defer func() {
		err := r.Delete(q.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(q.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	q2, err := r.Get(q.ID())
	require.Nil(t, err)
	require.NotNil(t, q2)
	require.Equal(t, q, q2)
	qm, err := r.GetMeta(q.ID())
	require.Nil(t, err)
	require.Equal(t, qm.ID(), q.ID())
	require.Equal(t, 3, qm.Options())
}
