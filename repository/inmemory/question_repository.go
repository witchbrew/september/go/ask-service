package inmemory

import (
	"github.com/pkg/errors"
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/september/go/ask-service/model/question"
	"gitlab.com/witchbrew/september/go/ask-service/model/question_meta"
	"gitlab.com/witchbrew/september/go/ask-service/repository"
)

type questionRepository struct {
	currentID uint64
	storage   map[uint64]*question.Question
}

func (r *questionRepository) Create(q *question.Question) error {
	if q.ID().IsInitialized() {
		return errors.Errorf("question ID is already initialized to %v", q.ID().Value)
	}
	r.currentID += 1
	q.ID().Value = r.currentID
	r.storage[r.currentID] = q
	return nil
}

func (r *questionRepository) Get(i *id.ID) (*question.Question, error) {
	if !i.IsInitialized() {
		return nil, id.ErrNotInitialized
	}
	key := i.Value.(uint64)
	q, ok := r.storage[key]
	if ok {
		return q, nil
	}
	return nil, nil
}

func (r *questionRepository) GetMeta(i *id.ID) (*question_meta.QuestionMeta, error) {
	q, err := r.Get(i)
	if err != nil {
		return nil, err
	}
	qm := question_meta.New(len(q.Options()))
	qm.ID().Value = q.ID().Value
	return qm, nil
}

func (r *questionRepository) Delete(i *id.ID) error {
	if !i.IsInitialized() {
		return id.ErrNotInitialized
	}
	key := i.Value.(uint64)
	delete(r.storage, key)
	return nil
}

func NewQuestionRepository() repository.Question {
	return &questionRepository{
		currentID: uint64(0),
		storage:   map[uint64]*question.Question{},
	}
}
