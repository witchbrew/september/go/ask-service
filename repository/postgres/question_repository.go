package postgres

import (
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/go/postgresutils"
	"gitlab.com/witchbrew/go/sqltemplate"
	"gitlab.com/witchbrew/go/sqlutils"
	"gitlab.com/witchbrew/september/go/ask-service/model/question"
	"gitlab.com/witchbrew/september/go/ask-service/model/question_meta"
	"gitlab.com/witchbrew/september/go/ask-service/repository"
	"gitlab.com/witchbrew/september/go/ask-service/sqlmeta"
	"gitlab.com/witchbrew/september/go/ask-service/sqlmetaimpl"
)

type questionRepository struct {
	db            *sql.DB
	questionTable *sqlmeta.QuestionTable
	optionTable   *sqlmeta.OptionTable
}

const optionsColumnsNum = 3

var insertQuestionT = sqltemplate.New(`
INSERT INTO {{t}} ({{textC}}) VALUES ($1) RETURNING {{idC}}
`)

var insertOptionsT = sqltemplate.New(`
INSERT INTO {{t}} ({{qIDC}}, {{indexC}}, {{textC}}) VALUES {{values}}
`)

func (r *questionRepository) Create(q *question.Question) error {
	if q.ID().IsInitialized() {
		return errors.Errorf("question ID is already initialized to %v", q.ID().Value)
	}
	if len(q.Options()) > 0 {
		tx, err := r.db.Begin()
		if err != nil {
			return errors.Wrap(err, "failed to start transaction")
		}
		questionQuery := insertQuestionT.Render(sqltemplate.Params{
			"t":     r.questionTable.Name,
			"textC": r.questionTable.Columns.Text,
			"idC":   r.questionTable.Columns.ID,
		})
		var qIDVal uint64
		err = tx.QueryRow(questionQuery, q.Text()).Scan(&qIDVal)
		if err != nil {
			return sqlutils.RollbackTxErrWrap(tx, err, "failed to scan question ID")
		}
		optionsQuery := insertOptionsT.Render(sqltemplate.Params{
			"t":      r.optionTable.Name,
			"qIDC":   r.optionTable.Columns.QuestionID,
			"indexC": r.optionTable.Columns.Index,
			"textC":  r.optionTable.Columns.Text,
			"values": postgresutils.InsertValues(optionsColumnsNum, len(q.Options())),
		})
		optionsValues := make([]interface{}, optionsColumnsNum*len(q.Options()))
		for index, option := range q.Options() {
			optionsValues[optionsColumnsNum*index] = qIDVal
			optionsValues[optionsColumnsNum*index+1] = index + 1
			optionsValues[optionsColumnsNum*index+2] = option
		}
		_, err = tx.Exec(optionsQuery, optionsValues...)
		if err != nil {
			return sqlutils.RollbackTxErrWrap(tx, err, "failed to insert options")
		}
		err = tx.Commit()
		if err != nil {
			return sqlutils.RollbackTxErrWrap(tx, err, "failed to commit transaction")
		}
		q.ID().Value = qIDVal
	} else {
		query := insertQuestionT.Render(sqltemplate.Params{
			"t":     r.questionTable.Name,
			"textC": r.questionTable.Columns.Text,
			"idC":   r.questionTable.Columns.ID,
		})
		var idVal uint64
		err := r.db.QueryRow(query, q.Text()).Scan(&idVal)
		if err != nil {
			return errors.Wrap(err, "failed to scan question ID")
		}
		q.ID().Value = idVal
	}
	return nil
}

var selectQuestionT = sqltemplate.New(`
SELECT {{textC}} FROM {{t}} WHERE {{idC}}=$1
`)

var selectOptionsT = sqltemplate.New(`
SELECT {{indexC}}, {{textC}} FROM {{t}} WHERE {{qIDC}}=$1
`)

func (r *questionRepository) Get(i *id.ID) (*question.Question, error) {
	if !i.IsInitialized() {
		return nil, id.ErrNotInitialized
	}
	questionQuery := selectQuestionT.Render(sqltemplate.Params{
		"textC": r.questionTable.Columns.Text,
		"t":     r.questionTable.Name,
		"idC":   r.questionTable.Columns.ID,
	})
	var questionText string
	err := r.db.QueryRow(questionQuery, i.Value).Scan(&questionText)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, errors.Wrap(err, "failed to scan question")
	}
	optionsQuery := selectOptionsT.Render(sqltemplate.Params{
		"indexC": r.optionTable.Columns.Index,
		"textC":  r.optionTable.Columns.Text,
		"t":      r.optionTable.Name,
		"qIDC":   r.optionTable.Columns.QuestionID,
	})
	rows, err := r.db.Query(optionsQuery, i.Value)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query options")
	}
	optsMap := map[int]string{}
	for rows.Next() {
		var index int
		var option string
		err = rows.Scan(&index, &option)
		if err != nil {
			return nil, sqlutils.CloseRowsErrWrapf(rows, err, "failed to scan option at %d", len(optsMap))
		}
		optsMap[index] = option
	}
	err = rows.Close()
	if err != nil {
		return nil, errors.Wrap(err, "failed to close options rows")
	}
	options := make([]string, len(optsMap))
	for index, o := range optsMap {
		options[index-1] = o
	}
	q := question.New(questionText, options...)
	q.ID().Value = i.Value
	return q, nil
}

var selectQuestionMetaT = sqltemplate.New(`
SELECT 1 FROM {{t}} WHERE {{idC}}=$1
`)

var selectOptionsMetaT = sqltemplate.New(`
SELECT COUNT({{idC}}) FROM {{t}} WHERE {{qIDC}}=$1
`)

func (r *questionRepository) GetMeta(i *id.ID) (*question_meta.QuestionMeta, error) {
	if !i.IsInitialized() {
		return nil, id.ErrNotInitialized
	}
	questionQuery := selectQuestionMetaT.Render(sqltemplate.Params{
		"t":   r.questionTable.Name,
		"idC": r.questionTable.Columns.ID,
	})
	var one int
	err := r.db.QueryRow(questionQuery, i.Value).Scan(&one)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, errors.Wrap(err, "failed to scan question")
	}
	optionsQuery := selectOptionsMetaT.Render(sqltemplate.Params{
		"idC":  r.optionTable.Columns.Index,
		"t":    r.optionTable.Name,
		"qIDC": r.optionTable.Columns.QuestionID,
	})
	var options int
	err = r.db.QueryRow(optionsQuery, i.Value).Scan(&options)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query count of options")
	}
	qm := question_meta.New(options)
	qm.ID().Value = i.Value
	return qm, nil
}

var deleteT = sqltemplate.New(`
DELETE FROM {{t}} WHERE {{idC}}=$1
`)

func (r *questionRepository) Delete(i *id.ID) error {
	if !i.IsInitialized() {
		return id.ErrNotInitialized
	}
	tx, err := r.db.Begin()
	if err != nil {
		return errors.Wrap(err, "failed to start transaction")
	}
	optionsQuery := deleteT.Render(sqltemplate.Params{
		"t":   r.optionTable.Name,
		"idC": r.optionTable.Columns.QuestionID,
	})
	_, err = tx.Exec(optionsQuery, i.Value)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to delete options")
	}
	questionQuery := deleteT.Render(sqltemplate.Params{
		"t":   r.questionTable.Name,
		"idC": r.questionTable.Columns.ID,
	})
	_, err = tx.Exec(questionQuery, i.Value)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to delete question")
	}
	err = tx.Commit()
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to commit")
	}
	return nil
}

func NewQuestionRepository(db *sql.DB) repository.Question {
	return &questionRepository{
		db:            db,
		questionTable: sqlmetaimpl.QuestionTable,
		optionTable:   sqlmetaimpl.OptionTable,
	}
}
