package repository

import (
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/september/go/ask-service/model/question"
	"gitlab.com/witchbrew/september/go/ask-service/model/question_meta"
)

type Question interface {
	Create(*question.Question) error
	Get(*id.ID) (*question.Question, error)
	GetMeta(*id.ID) (*question_meta.QuestionMeta, error)
	Delete(*id.ID) error
}
